class Box() :
    def __init__(self, capa = None):
        self._contents = []
        self._open = False
        self._capacity = capa

    def __contains__(self,item):
        return item in self._contents

    def add(self, item):
        self._contents.append(item)

    def remove(self,item):
        self._contents.remove(item)

    def is_open(self):
        return self._open

    def open(self):
        if not self.is_open():
            self._open = True

    def close(self):
        if self.is_open():
            self._open = False

    def action_look(self):
        res = ""
        if not self.is_open():
            res = "la boite est fermée"
        else:
            res = "la boite contient : "+", ".join(self._contents)
        return res
    def set_capacity(self,n):
        self._capacity = n

    def capacity(self):
        return self._capacity

    def has_room(self, item):
        if self._capacity == None:
            return True
        else :
            return self._capacity > item._volume

    def add_item(self,item):
        ajout = False
        if self.has_room(item) and self.is_open():
            self.add(item)
            if self._capacity != None:
                self.set_capacity(self.capacity()-item.volume())
            ajout = True
        return ajout

    def find(self,name):
        find = False
        if not self.is_open() :
            find = None
        else :
            for item in self._contents:
                if item.has_name(name):
                    find = True
        return find

class Item() :
    def __init__(self,space = None, repres = ""):
        self._volume = space
        self._repr = repres

    def __repr__(self):
        return "Objet : "+str(self._repr)

    def volume(self):
        return self._volume

    def set_name(self,name):
        self._repr = name

    def has_name(self, name):
        return self._repr == name

class Person():
    def __init__(self, firstname=None, lastname=None):
        self._firstname = firstname
        self._lastname = lastname

    @staticmethod
    def from_data(data):
        f = data.get("firstname", None)
        l = data.get("lastname", None)
        return Person(firstname=f,lastname=l)
